import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class TestRuleta 
{
    /**
     * Default constructor for test class TestRuleta
     */
    Ruleta r;
    
    public TestRuleta()
    {

    }

    /**
     * Sets up the test fixture.
     *
     * Called before every test case method.
     */
    @Before
    public void setUp()
    {

    }

    /**
     * Tears down the test fixture.
     *
     * Called after every test case method.
     */
    @After
    public void tearDown()
    {

    }

    /*
     * CASOS DE BASE
     */
    @Test
    public void test_siDineroDisponibleCorrecto() {
        
        r= new Ruleta(1,100,5);
        
        assertEquals("true",r.comprobarDineroDisponible());
    }
    
    @Test
    public void test_siDineroDisponibleNOCorrecto() {
        
        r= new Ruleta(1,4,5);
        
        assertEquals("false", r.comprobarDineroDisponible());
    }
    
    @Test
    public void test_siDineroApostadoCorrecto() {
        
        r= new Ruleta(1,100,5);
        
        assertEquals("true", r.comprobarDineroApostado());
    }
    
    @Test
    public void test_siDineroApostadoNOCorrecto() {
        
        r= new Ruleta(1,100,4);
        
        assertEquals("false", r.comprobarDineroApostado());
    }
    
    @Test
    public void test_siDineroDisponibleCorrecto_siDineroApostadoCorrecto() {
        
        r= new Ruleta(1,100,5);
                
        assertEquals("true", r.comprobarDineroDisponible());
        assertEquals("true", r.comprobarDineroApostado());
    }
    
    @Test
    public void test_siDineroDisponibleCorrecto_siDineroApostadoNoCorrecto() {
        
        r= new Ruleta(1,100,4);
                
        assertEquals("true", r.comprobarDineroDisponible());
        assertEquals("false", r.comprobarDineroApostado());
    }
    
    
    @Test
    public void testCrearPremioAleatorio() {
        
        r= new Ruleta(1,100,5);
                        
        assertEquals("true", r.aleatorioValido());
    }
    
    /*
     * CASOS DE PREMIOS
     */
    
    //1.rojo al menor par
    @Test
    public void test_rojo_menor_par() {
        
        r= new Ruleta(12,100,5);
        //aseguramos el premio para ver si es valido el test
        r.setPremio(12);
        String resp = r.comprobarPremio();
        
        assertEquals("Premio rojo apostado al menor en numero par", resp);
    }
    
    //2.rojo al menor impar
    @Test
    public void test_rojo_menor_impar() {
        r= new Ruleta(1,100,5);
        //aseguramos el premio para ver si es valido el test
        r.setPremio(1);
        String resp = r.comprobarPremio();
        
        assertEquals("Premio rojo apostado al menor en numero impar", resp);
    }
    
    //3.rojo al mayor par
    @Test
    public void test_rojo_mayor_par() {
        r= new Ruleta(30,100,5);
        //aseguramos el premio para ver si es valido el test
        r.setPremio(30);
        String resp = r.comprobarPremio();
        
        assertEquals("Premio rojo apostado al mayor en numero par", resp);
    }
    
    //4.rojo al mayor impar
    @Test
    public void test_rojo_mayor_impar() {
        r= new Ruleta(19,100,5);
        //aseguramos el premio para ver si es valido el test
        r.setPremio(19);
        String resp = r.comprobarPremio();
        
        assertEquals("Premio rojo apostado al mayor en numero impar", resp);
    }
    
    //5.negro al menor par
    @Test
    public void test_negro_menor_par() {
        r= new Ruleta(2,100,5);
        //aseguramos el premio para ver si es valido el test
        r.setPremio(2);
        String resp = r.comprobarPremio();
        
        assertEquals("Premio negro apostado al menor en numero par", resp);
    }
    
    //6.negro al menor impar
    @Test
    public void test_negro_menor_impar() {
        r= new Ruleta(11,100,5);
        //aseguramos el premio para ver si es valido el test
        r.setPremio(11);
        String resp = r.comprobarPremio();
        
        assertEquals("Premio negro apostado al menor en numero impar", resp);
    }
    
    //7.negro al mayor par
    @Test
    public void test_negro_mayor_par() {
        r= new Ruleta(20,100,5);
        //aseguramos el premio para ver si es valido el test
        r.setPremio(20);
        String resp = r.comprobarPremio();
        
        assertEquals("Premio negro apostado al mayor en numero par", resp);
    }
    
    //8.negro al mayor impar
    @Test
    public void test_negro_mayor_impar() {
        r= new Ruleta(29,100,5);
        //aseguramos el premio para ver si es valido el test
        r.setPremio(29);
        String resp = r.comprobarPremio();
        
        assertEquals("Premio negro apostado al mayor en numero impar", resp);
    }
    
    //9.verde
    @Test
    public void test_verde() {
        r= new Ruleta(0,100,5);
        //aseguramos el premio para ver si es valido el test
        r.setPremio(0);
        String resp = r.comprobarPremio();
        
        assertEquals("Premio verde", resp);
    }
    
    //10.verde
    @Test
    public void test_noPremio() {
        r= new Ruleta(2,100,5);
        //aseguramos el premio para ver si es valido el test
        r.setPremio(0);
        String resp = r.comprobarPremio();
        
        assertEquals("No tienes premio", resp);
    }
    
    /*
     * CASO SI NO GANA QUE ES VALIDO EN CUALQUIER SITUACION
     */
    @Test
    public void test_premioDisponible_siNoGana() {
        
        int dis= 100;
        int apo = 5;
        
        r= new Ruleta(0,dis,apo);
        //aseguramos el premio para ver si es valido el test
        r.setPremio(1);
        r.comprobarPremio();
        dis -= apo;
        int resp = r.getDineroDisponible();
                
        assertEquals(dis, resp);
    }
    
    /*
     * CASOS DINERO DISPONIBLE AL GANAR
     */
    @Test
    public void test_premioDisponible_siGanaVerde() {
        
        int dis= 100;
        int apo = 5;
        
        r= new Ruleta(0,dis,apo);
        //aseguramos el premio para ver si es valido el test
        r.setPremio(0);
        r.comprobarPremio();
        
        //obtenemos el dinero disponible una vez que decrementamos lo apostado
        dis -= apo;
        dis += apo*3;
        
        int resp = r.getDineroDisponible();
                
        assertEquals(dis, resp);
    }
    
    //es valido para toda combinacion de rojo, siempre es doble  ********SE PUEDEN HACER TODOS LOS CASOS
    //rojo-menor-par   rojo-menor-impar   rojo-mayor-par   rojo-mayor-impar
    @Test
    public void test_premioDisponible_siGanaRojo() {
        
        int dis= 100;
        int apo = 5;
        
        r= new Ruleta(1,dis,apo);
        //aseguramos el premio para ver si es valido el test
        r.setPremio(1);
        r.comprobarPremio();
        
        //obtenemos el dinero disponible una vez que decrementamos lo apostado
        dis -= apo;
        dis += apo*2;
        
        int resp = r.getDineroDisponible();
                
        assertEquals(dis, resp);
    }
    
    //es valido para toda combinacion de negro, siempre es doble
    @Test
    public void test_premioDisponible_siGanaNegro() {
        
        int dis = 100;
        int apo = 5;
        
        r= new Ruleta(2,dis,apo);
        //aseguramos el premio para ver si es valido el test
        r.setPremio(2);
        r.comprobarPremio();
        
        //obtenemos el dinero disponible una vez que decrementamos lo apostado
        dis -= apo;
        dis += apo*2;
        
        int resp = r.getDineroDisponible();
                
        assertEquals(dis, resp);
    }
    
    /*
     * CASOS METODOS AUXILIARES
     */
    @Test
    public void test_siEsPar() {
        r= new Ruleta(2,100,5);
                
        assertEquals("true", r.par(2));
    }
    
    @Test
    public void test_siNoPar() {
        r= new Ruleta(3,100,5);
                
        assertEquals("false", r.par(3));
    }
    
    @Test
    public void test_siIgual() {
        r= new Ruleta(2,100,5);
        r.setPremio(2); 
        int p = r.getPremio();
        assertEquals("true", r.esIgual(2,p));
    }
    
    @Test
    public void test_esIgual() {
        r = new Ruleta(2,100,5);
        r.setPremio(10);
        int p = r.getPremio();
        assertEquals("false", r.esIgual(2,p));
    }
    
    @Test
    public void test_siMenor() {
        r= new Ruleta(2,100,5);
                
        assertEquals("true", r.esMenor(2));
        
    }
    
    @Test
    public void test_noMenor() {
        r= new Ruleta(20,100,5);
        
        assertEquals("false", r.esMenor(20));
    }
    
    /*
     * CASOS GET Y SET
     */
    //GET
    @Test
    public void test_getDineroDisponible() {
        
        int dis= 100;
        int apo = 5;
        
        r= new Ruleta(20,dis,apo);
        int res= r.getDineroDisponible();
        dis -= apo;
        
        assertEquals(dis,res);
    }
    
    @Test
    public void test_getDineroApostado() {
        r= new Ruleta(20,100,5);
        int res= r.getDineroApostado();
        
        assertEquals(5,res);
    }
    
    @Test
    public void test_getNum() {
        r= new Ruleta(20,100,5);
        int res= r.getNum();
        
        assertEquals(20,res);
    }
    
    @Test
    public void test_getPremio() {
        r= new Ruleta(20,100,5);
        int res= r.getPremio();
        boolean valido = (res >= 0) && (res <= 37);
        
        assertTrue(valido);
    }
    
    //SET
    @Test
    public void test_setDineroDisponible() {
        r= new Ruleta(20,100,5);
        r.setDineroDisponible(200);
        int res = r.getDineroDisponible();
        
        assertEquals(200,res);
    }
    
    @Test
    public void test_setDineroApostado() {
        r= new Ruleta(20,100,5);
        r.setDineroApostado(10);
        int res = r.getDineroApostado();
        
        assertEquals(10,res);
    }
    
    @Test
    public void test_setNum() {
        r= new Ruleta(20,100,5);
        r.setNum(0);
        int res = r.getNum();
        
        assertEquals(0,res);
    }
    
    @Test
    public void test_setPremio() {
        r= new Ruleta(20,100,5);
        r.setPremio(0);
        int res= r.getPremio();
        
        assertEquals(0,res);
    }

}


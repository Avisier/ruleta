 

import java.util.Random;

public class Ruleta {

	int num,premio;
	int dineroDisponible, dineroApostado;
	int rojo[]= {1,3,5,7,9,12,14,16,18,19,21,23,25,27,30,32,34,36};
	int negro[] = {2,4,6,8,10,11,15,17,20,22,24,26,28,29,31,33,35};
		
	static final int VERDE = 0;
	static final int MENOR = 18;
	
	public Ruleta(int num, int disp, int apo) {
		this.num = num;
		this.dineroDisponible = disp;
		this.dineroApostado = apo;
		
		comprobarDineroDisponible();
		comprobarDineroApostado();
		this.dineroDisponible -= this.dineroApostado;
		
		this.premio = crearPremioAleatorio();
		comprobarPremio();
		
	}
	
	public boolean comprobarDineroDisponible() {
		/*
		 * if(this.dineroDisponible >= 5)
		 * 		return true;
		 * else
		 * 		return false;
		 */
		return this.dineroDisponible >= 5;
	}
	
	public boolean comprobarDineroApostado() {
		
		/*
		 * if((this.dineroApostado >= 5) && (this.dineroApostado <= this.dineroDisponible))
		 * 		return true;
		 * else 
		 * 		return false;
		 */
		return (this.dineroApostado >= 5) && (this.dineroApostado <= this.dineroDisponible);
	}
	
	public int crearPremioAleatorio() {
		Random ale = new Random();
		return ale.nextInt(37);
	}
		
	public String comprobarPremio() {
		
			if(esIgual(this.num,VERDE) && esIgual(this.num,premio)) {
				this.dineroDisponible += (this.dineroApostado*3);
				return "Premio verde";
			}
			
			for(int i = 0; i < rojo.length; i++) {
				
				if(esIgual(this.num,rojo[i]) && esIgual(this.num,premio)) {
					
					this.dineroDisponible += (this.dineroApostado*2);
					
					if(esMenor(this.num) && par(rojo[i])) 
						return "Premio rojo apostado al menor en numero par";
					
					if(esMenor(this.num) && !par(rojo[i])) 
						return "Premio rojo apostado al menor en numero impar";
					
					if(!esMenor(this.num) && par(rojo[i]))
						return "Premio rojo apostado al mayor en numero par";
					
					if(!esMenor(this.num) && !par(rojo[i]))
						return "Premio rojo apostado al mayor en numero impar";
				}
		
			}//numerosRojos
			
			for(int i = 0; i < negro.length; i++) {
				
				if(esIgual(this.num,negro[i]) && esIgual(this.num,premio)) {
					
					this.dineroDisponible += (this.dineroApostado*2);
					
					if(esMenor(this.num) && par(negro[i])) 
						return "Premio negro apostado al menor en numero par";
					
					if(esMenor(this.num)&& !par(negro[i])) 
						return "Premio negro apostado al menor en numero impar";
					
					if(!esMenor(this.num)&& par(negro[i]))
						return "Premio negro apostado al mayor en numero par";
					
					if(!esMenor(this.num) && !par(negro[i]))
						return "Premio negro apostado al mayor en numero impar";
				}
		
			}//numerosNegros
			
		return "No tienes premio";
	}
	
	/**
	 * METODOS AUXILIARES
	 */
	
	public boolean par(int num) {
		
		 if(num % 2 == 0)
			 return true;
		 else
			 return false;
	}
	
	public boolean esMenor(int num) {
		
		 if(num <= MENOR)
			 return true;
		 else
			 return false;
	}
	
	public boolean esIgual(int num1, int num2) {
		
		 if(num1 == num2)
			 return true;
		 else
			 return false;
	}
	
	/**
	 * GET y SET
	 */
	
	//GET
	public int getDineroApostado() {
		return dineroApostado;
	}
	
	public int getDineroDisponible() {
		return dineroDisponible;
	}
	
	public int getNum() {
		return num;
	}
	
	public int getPremio() {
		return premio;
	}
	
	//SET
	public void setDineroApostado(int dineroApostado) {
		this.dineroApostado = dineroApostado;
	}
	
	public void setDineroDisponible(int dineroDisponible) {
		this.dineroDisponible = dineroDisponible;
	}
	
	public void setNum(int num) {
		this.num = num;
	}
	
	public void setPremio(int premio) {
		this.premio = premio;
	}
	
}

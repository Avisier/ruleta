import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class TestRuleta 
{
    /**
     * Default constructor for test class TestRuleta
     */
    Ruleta r;
    
    public TestRuleta()
    {

    }

    /**
     * Sets up the test fixture.
     *
     * Called before every test case method.
     */
    @Before
    public void setUp()
    {

    }

    /**
     * Tears down the test fixture.
     *
     * Called after every test case method.
     */
    @After
    public void tearDown()
    {

    }

    /*
     * CASOS DE BASE
     */
    @Test
    public void test_siDineroDisponibleCorrecto() {
        
        r= new Ruleta(1,100,5);
        
        assertEquals("true",r.comprobarDineroDisponible());
    }
    
    @Test
    public void test_siDineroDisponibleNOCorrecto() {
        
        r= new Ruleta(1,4,5);
        
        assertEquals("false", r.comprobarDineroDisponible());
    }
    
    @Test
    public void test_siDineroApostadoCorrecto() {
        
        r= new Ruleta(1,100,5);
        
        assertEquals("true", r.comprobarDineroApostado());
    }
    
    @Test
    public void test_siDineroApostadoNOCorrecto() {
        
        r= new Ruleta(1,100,4);
        
        assertEquals("false", r.comprobarDineroApostado());
    }
    
    @Test
    public void test_siDineroDisponibleCorrecto_siDineroApostadoCorrecto() {
        
        r= new Ruleta(1,100,5);
                
        assertEquals("true", r.comprobarDineroDisponible());
        assertEquals("true", r.comprobarDineroApostado());
    }
    
    @Test
    public void test_siDineroDisponibleCorrecto_siDineroApostadoNoCorrecto() {
        
        r= new Ruleta(1,100,4);
                
        assertEquals("true", r.comprobarDineroDisponible());
        assertEquals("false", r.comprobarDineroApostado());
    }
    
    
    @Test
    public void testCrearPremioAleatorio() {
        
        r= new Ruleta(1,100,5);
                        
        assertEquals("true", r.aleatorioValido());
    }
    
    /*
     * CASOS DE PREMIOS
     */
    
    //1.rojo al menor par
    @Test
    public void test_rojo_menor_par() {
        
        r= new Ruleta(12,100,5);
        //aseguramos el premio para ver si es valido el test
        r.setPremio(12);
        String resp = r.comprobarPremio();
        
        assertEquals("Premio rojo apostado al menor en numero par", resp);
    }
    
    //2.rojo al menor impar
    @Test
    public void test_rojo_menor_impar() {
        r= new Ruleta(1,100,5);
        //aseguramos el premio para ver si es valido el test
        r.setPremio(1);
        String resp = r.comprobarPremio();
        
        assertEquals("Premio rojo apostado al menor en numero impar", resp);
    }
    
    //3.rojo al mayor par
    @Test
    public void test_rojo_mayor_par() {
        r= new Ruleta(30,100,5);
        //aseguramos el premio para ver si es valido el test
        r.setPremio(30);
        String resp = r.comprobarPremio();
        
        assertEquals("Premio rojo apostado al mayor en numero par", resp);
    }
    
    //4.rojo al mayor impar
    @Test
    public void test_rojo_mayor_impar() {
        r= new Ruleta(19,100,5);
        //aseguramos el premio para ver si es valido el test
        r.setPremio(19);
        String resp = r.comprobarPremio();
        
        assertEquals("Premio rojo apostado al mayor en numero impar", resp);
    }
    
    //5.negro al menor par
    @Test
    public void test_negro_menor_par() {
        r= new Ruleta(2,100,5);
        //aseguramos el premio para ver si es valido el test
        r.setPremio(2);
        String resp = r.comprobarPremio();
        
        assertEquals("Premio negro apostado al menor en numero par", resp);
    }
    
    //6.negro al menor impar
    @Test
    public void test_negro_menor_impar() {
        r= new Ruleta(11,100,5);
        //aseguramos el premio para ver si es valido el test
        r.setPremio(11);
        String resp = r.comprobarPremio();
        
        assertEquals("Premio negro apostado al menor en numero impar", resp);
    }
    
    //7.negro al mayor par
    @Test
    public void test_negro_mayor_par() {
        r= new Ruleta(20,100,5);
        //aseguramos el premio para ver si es valido el test
        r.setPremio(20);
        String resp = r.comprobarPremio();
        
        assertEquals("Premio negro apostado al mayor en numero par", resp);
    }
    
    //8.negro al mayor impar
    @Test
    public void test_negro_mayor_impar() {
        r= new Ruleta(29,100,5);
        //aseguramos el premio para ver si es valido el test
        r.setPremio(29);
        String resp = r.comprobarPremio();
        
        assertEquals("Premio negro apostado al mayor en numero impar", resp);
    }
    
    //9.verde
    @Test
    public void test_verde() {
        r= new Ruleta(0,100,5);
        //aseguramos el premio para ver si es valido el test
        r.setPremio(0);
        String resp = r.comprobarPremio();
        
        assertEquals("Premio verde", resp);
    }
    
    //10.verde
    @Test
    public void test_noPremio() {
        r= new Ruleta(2,100,5);
        //aseguramos el premio para ver si es valido el test
        r.setPremio(0);
        String resp = r.comprobarPremio();
        
        assertEquals("No tienes premio", resp);
    }
}  
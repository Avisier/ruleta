import java.util.Random;
/**
 * Write a description of class rule here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class rule
{
    // instance variables - replace the example below with your own
    int num;
    int numApostado;
    int premio;
    int rojo[] = {1, 3, 5, 7, 9, 12, 14, 16, 18, 21, 23, 25, 27, 28, 30, 32, 34, 36}; 
    int negro[] = {2, 4, 6, 8, 10, 11, 13, 15, 17, 19, 20, 22, 24, 26, 29, 31, 33, 35};
    int par[]= {2, 4, 6, 8, 10, 12, 14, 16, 18, 20, 22, 24, 26, 28, 30, 32, 34, 36};
    int impar[] = {1, 3, 5, 7, 9, 11, 13, 15, 17, 19, 21, 23, 25, 27, 29, 31, 33, 35};
    static final int verde = 0;
    static final int menor = 18;
    static final int mayor = 19;
    
    
    /**
     * Constructor for objects of class rule
     */
    public rule()
    {
        // initialise instance variables
        num = 1;
            
    }
    
    public String dimeNum(){
    
        // Número aleatorio entre 0-36
        
        {
        Random Aleatorio = new Random();     
        int num = Aleatorio.nextInt(37);
        System.out.println("El numero es: " + num);
        }
        
        //Comprobacion de apuestas
        
        if (num == numApostado){
            
            //Apuesta al verde = 0
            
            if (num == verde)
                System.out.println("Premio verde");
            else
                System.out.println("No has ganado, vuelve a apostar");
            
            //Apuestas a rojo, r_par, r_impar, r_mayor y r_menor
                
            for(int r=0; r<rojo.length; r++)
                for(int p=0; p<par.length; p++)
                    for(int i=0; i<impar.length; i++){
                
                        if(num == rojo[r])
                            System.out.println("Premio rojo");
                        else
                            System.out.println("No has ganado, vuelve a apostar");
            
                        if(num == rojo[r] && num <= menor)
                            System.out.println("Premio rojo y menor");
                        else
                            System.out.println("No has ganado, vuelve a apostar");
            
                        if (num == rojo[r] && num >= mayor)
                            System.out.println("Premio rojo y mayor");
                        else
                            System.out.println("No has ganado, vuelve a apostar");
                    
                        if(num == rojo[r] && num == par[p])
                            System.out.println("Premio rojo y par");
                        else
                            System.out.println("No has ganado, vuelve a apostar");
                    
                        if(num == rojo[r] && num == impar[i])
                            System.out.println("Premio rojo e impar");
                        else
                            System.out.println("No has ganado, vuelve a apostar");
                    }
            
            //Apuestas a negro y n_par, n_impar, n_mayor y n_menor
            
            for(int n=0; n<negro.length; n++)
                for(int p=0; p<par.length; p++)
                    for(int i=0; i<impar.length; i++){
                
                        if(num == negro[n])
                            System.out.println("Premio negro");
                        else 
                            System.out.println("No has ganado, vuelve a apostar");
                
                        if(num == negro[n] && num <= menor)
                            System.out.println("Premio negro y menor");
                        else 
                            System.out.println("No has ganado, vuelve a apostar");
                    
                        if(num == negro[n] && num >= mayor)
                            System.out.println("Premio negro y mayor");
                        else
                            System.out.println("No has ganado, vuelve a apostar");
                    
                        if(num == negro[n] && num == par[p])
                            System.out.println("Premio negro y par");
                        else
                            System.out.println("No has ganado, vuelve a apostar");
                    
                        if(num == negro[n] && num == impar[i])
                            System.out.println("Premio negro e impar");
                        else
                            System.out.println("No has ganado, vuelve a apostar");
                    }
            
            //Apuestas a par e impar
            
            for(int p=0; p<par.length; p++)
                for(int i=0; i<impar.length; i++){
                
                    if(num == par[p])
                        System.out.println("Premio par");
                    else 
                        System.out.println("No has ganado, vuelve a apostar");
                        
                    if(num == impar[i])
                        System.out.println("Premio impar");
                    else 
                        System.out.println("No has ganado, vuelve a apostar");
                }
        
        }       
    }
}

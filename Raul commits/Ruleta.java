import java.util.Random;
import java.util.Scanner;

public class Ruleta {

    int numUsuario,numPremiado;
    int dineroDisponible,dineroApostado;
    int premio;
    int rojo[]= {1,3,5,7,9,12,14,16,18,19,21,23,25,27,30,32,34,36};
    int negro[] = {2,4,6,8,10,11,15,17,20,22,24,26,28,29,31,33,35};
    static final int VERDE = 0;
    static final int MENOR = 18;
    static final int MAYOR = 19;
    
    public Ruleta() {
        
        this.dineroDisponible = 0;
        this.dineroApostado = 0;
        
        apuestaInicial();
        numeroApuestas();
    }
    
    public void apuestaInicial() {
        
        while(this.dineroDisponible < 5) {
            
            System.out.print("Cuanto dinero quieres apostar:");
            Scanner sc=new Scanner(System.in);
            this.dineroDisponible = sc.nextInt();
            
            if(this.dineroDisponible < 5)
                System.out.println("La cantidad debe de ser mayor o igual que 5");
            
        }//while
    }
    
    public int sacarPremio() {
        Random ale = new Random();
        return ale.nextInt(37);
    }
    
    public void apostarNumero() {
        
        System.out.print("Numero que apuestas:");
        Scanner sc=new Scanner(System.in);
        this.numUsuario = sc.nextInt();
    }
    
    public void apostarDinero() {
        
        this.dineroApostado = 0;
        
        while(this.dineroApostado < 5 && this.dineroApostado <= this.dineroDisponible) {
            System.out.print("Cuanto dinero quieres apostar:");
            Scanner sc=new Scanner(System.in);
            this.dineroApostado = sc.nextInt();
            
            if(this.dineroApostado < 5)
                System.out.println("La cantidad debe de ser mayor o igual que 5");
            else if(this.dineroApostado > this.dineroDisponible)
                System.out.println("La cantidad debe de ser menor o igual "+this.dineroDisponible);
            
        }//while
        
        this.dineroDisponible -= this.dineroApostado;
    }

    